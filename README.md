# Battleship
____
## What is this project about
This is the personal project to master skills of creator in web development and microsevice achitecture.
Project represents very well known real time game named Battleship.
You can login/register here via plain email registration or play as a guest if you don't want to create account, but you won't be able to see you game history or stats.
### Technical stack
For this project I'm planning to use:
1. docker to run all the services isolated and in one click
2. Node.js and REST API for backend
3. Rabbitmq to send emails to user (a little overhead, but it's just for practice)
4. React.js for frontend
5. Postgresql to contain data
6. Nginx to manage service access
7. ?Automatic CI/CD?
#### Backend
Sevices will communicate with each other via http request/responce, except sending emails - this will be handeled in Rabbitmq. 
List of services:
1. auth - handles all the stuff for user auth
2. email - handles sending email for account confirmation/password reset
3. battleship - all the stuff related to the game
#### Database schema
List of tables:
1. user
	- id
	- username
	- email
	- first_name
	- last_name
	- avatar
	- email_confirmed
	- password_hash
	- joined_at
	- last_online
	- is_guest
2. user_token
	- id
	- token
	- user_id
	- expired_at
3. expiring_user_data
	- data
	- expired_at
4. game
	- started_at
	- winner_id
	...
5. user_game
	- user_id
	- game_id
	- board_state
6. real_time_game
	- time_to_next_move
	- game_id
	- user_turn_id